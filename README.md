# OxiBagel: rewriting Lox/Bagel in Rust

[![pipeline status](https://gitlab.com/jcgoble3/oxibagel/badges/main/pipeline.svg)](https://gitlab.com/jcgoble3/oxibagel/-/commits/main)

This is part 2 of my independent study toward my computer science degree at Wright State University. What began as [an implementation of the Lox language](https://gitlab.com/jcgoble3/cbagel) described in Part III of the book
[*Crafting Interpreters*](https://craftinginterpreters.com/), then adding some of my own ideas along the way and attempting to turn it into a full-featured scripting language with a new name: CBagel, or just Bagel (in the same spirit and sense of CPython and Pythom, in that Bagel is the language, and CBagel is the reference implementation, kinda like Python and CPython), hit a roadblock with frustration over garbage collection issues. I decided after discussing with my professor to rewrite the base version of Lox from the book in Rust to take advantage of the memory safety guarantees it makes.

For notes specifically about Rust that I compiled while working through the book, see [the notes folder](notes/index.md). For note's about general language implementation, see the CBagel repo linked above.

## Building

This implementation uses Cargo and its usual commands as a build system. A simple Makefile is also provided for those who find more comfort in typing `make`.
