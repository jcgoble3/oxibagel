//use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use std::time;

use crate::chunk::Op;
use crate::compiler;
use crate::value;
use crate::value::Value;

const MAX_FRAMES: usize = 1000;

pub enum InterpretResult {
    Ok,
    CompileError,
    RuntimeError,
}

struct CallFrame {
    closure: Rc<value::Closure>,
    ip: usize,
    fp: usize,
}

pub struct VM {
    start_time: time::Instant,
    frames: Vec<CallFrame>,
    stack: Vec<Value>,
    globals: HashMap<String, Value>,
    open_upvalues: Option<value::UpvaluePtr>,
}

macro_rules! runtime_error {
    ($self:expr, $fmt:expr $(,$args:expr)*) => {{
        eprintln!($fmt $(,$args)*);
        for frame in $self.frames.iter().rev() {
            let function = &frame.closure.function;
            eprint!("[line {}] in ", function.chunk.lines[frame.ip - 1]);
            if function.name == "" {
                eprintln!("script");
            } else {
                eprintln!("{}()", function.name);
            }
        }
        $self.reset_stack();
    }};
}

impl VM {
    pub fn new() -> VM {
        let mut vm = VM {
            start_time: time::Instant::now(),
            frames: Vec::new(),
            stack: Vec::new(),
            globals: HashMap::new(),
            open_upvalues: None,
        };
        vm.define_native("clock".to_string(), clock_native, 0);
        vm
    }

    fn frame_mut(&mut self) -> &mut CallFrame {
        self.frames.last_mut().unwrap()
    }

    fn frame(&self) -> &CallFrame {
        self.frames.last().unwrap()
    }

    fn slot(&mut self, index: usize) -> &mut Value {
        let abs_index = self.frame().fp + index;
        &mut self.stack[abs_index]
    }

    fn push(&mut self, value: Value) {
        self.stack.push(value);
    }

    fn pop(&mut self) -> Value {
        self.stack.pop().unwrap()
    }

    fn peek(&self, distance: usize) -> Value {
        self.stack[self.stack.len() - distance - 1].clone()
    }

    fn reset_stack(&mut self) {
        self.stack.clear()
    }

    pub fn interpret(&mut self, source: Vec<u8>) -> InterpretResult {
        let function = match compiler::compile(source) {
            None => return InterpretResult::CompileError,
            Some(func) => func,
        };
        let closure = value::wrap_closure(function);
        self.push(Value::Closure(Rc::clone(&closure)));
        self.call(closure, 0);
        self.run()
    }

    fn run(&mut self) -> InterpretResult {

        macro_rules! binary_op {
            // We need double braces here: the outer set is the macro block
            // itself, while the inner set is a literal set substituted with the
            // macro invocation
            ($wrapper:expr, $op:tt) => {{
                if let (Value::Number(b), Value::Number(a)) = (self.peek(0), self.peek(1)) {
                    self.pop();
                    self.pop();
                    self.push($wrapper(a $op b));
                } else {
                    runtime_error!(self, "Operands must be numbers.");
                    return InterpretResult::RuntimeError;
                }
            }};
        }

        macro_rules! get_upvalue {
            ($upvalue:expr) => {
                match $upvalue.state {
                    value::UpvalueState::Open(index) => &mut self.stack[index],
                    value::UpvalueState::Closed(ref mut value) => value,
                }
            };
        }

        loop {
            #[cfg(feature = "vmtrace")] {
                print!("          ");
                for slot in self.stack.iter() {
                    print!("[ {} ]", *slot);
                }
                println!("");
                self.frame().closure.function.chunk.dis_instruction(self.frame().ip);
            }

            let instruction = self.frame().closure.function.chunk.code[self.frame().ip];
            self.frame_mut().ip += 1;
            match instruction {

                Op::Constant(k) => {
                    let constant = self.get_constant(k);
                    self.push(constant);
                }

                Op::Nil() => self.push(Value::Nil),

                Op::True() => self.push(Value::Bool(true)),

                Op::False() => self.push(Value::Bool(false)),

                // self.pop() returns a value and so causes a compiler error if
                // we try to use it like an expression because of incompatible
                // types on the match arms
                Op::Pop() => {
                    self.pop();
                }

                Op::GetLocal(slot) => {
                    let value = self.slot(slot as usize).clone();
                    self.push(value);
                }

                Op::SetLocal(slot) => {
                    let value = self.peek(0).clone();
                    *self.slot(slot as usize) = value;
                }

                Op::GetGlobal(k) => {
                    let name = self.get_string(k);
                    match self.globals.get(&name) {
                        Some(value) => {
                            let value = value.clone();
                            self.push(value);
                        }
                        None => {
                            runtime_error!(self, "Undefined variable '{}'.", name);
                            return InterpretResult::RuntimeError;
                        }
                    }
                }

                Op::DefineGlobal(k) => {
                    let name = self.get_string(k);
                    let value = self.pop();
                    self.globals.insert(name, value);
                }

                Op::SetGlobal(k) => {
                    let name = self.get_string(k);
                    let value = self.peek(0);
                    if self.globals.contains_key(&name) {
                        self.globals.insert(name, value);
                    } else {
                        runtime_error!(self, "Undefined variable '{}'.", name);
                        return InterpretResult::RuntimeError;
                    }
                }

                Op::GetUpvalue(i) => {
                    let upvalue = Rc::clone(&self.frame().closure.upvalues[i as usize]);
                    let mut upvalue = upvalue.borrow_mut();
                    let value = get_upvalue!(upvalue).clone();
                    self.push(value);
                }

                Op::SetUpvalue(i) => {
                    let upvalue = Rc::clone(&self.frame().closure.upvalues[i as usize]);
                    let mut upvalue = upvalue.borrow_mut();
                    *get_upvalue!(upvalue) = self.peek(0);
                }

                Op::Equal() => {
                    let b = self.pop();
                    let a = self.pop();
                    self.push(Value::Bool(a == b));
                }

                Op::NotEqual() => {
                    let b = self.pop();
                    let a = self.pop();
                    self.push(Value::Bool(a != b));
                }

                Op::Greater() => binary_op!(Value::Bool, >),

                Op::GreaterEqual() => binary_op!(Value::Bool, >=),

                Op::Less() => binary_op!(Value::Bool, <),

                Op::LessEqual() => binary_op!(Value::Bool, <=),

                Op::Add() => {
                    match (self.peek(1), self.peek(0)) {
                        (Value::String(_), Value::String(_)) => {
                            let b = self.pop();
                            let a = self.pop();
                            let result = format!("{}{}", a, b);
                            self.push(value::new_string(result));
                        }
                        (Value::Number(a), Value::Number(b)) => {
                            self.pop();
                            self.pop();
                            self.push(Value::Number(a + b));
                        }
                        _ => {
                            runtime_error!(self, "Operands must be two numbers or two strings.");
                            return InterpretResult::RuntimeError;
                        }
                    }
                }

                Op::Subtract() => binary_op!(Value::Number, -),

                Op::Multiply() => binary_op!(Value::Number, *),

                Op::Divide() => binary_op!(Value::Number, /),

                Op::Not() => {
                    let value = self.pop();
                    self.push(Value::Bool(value.is_falsey()));
                }

                Op::Negate() => {
                    match self.peek(0) {
                        Value::Number(n) => {
                            self.pop();
                            self.push(Value::Number(-n));
                        }
                        _ => {
                            runtime_error!(self, "Operand must be a number.");
                            return InterpretResult::RuntimeError;
                        }
                    }
                }

                Op::Print() => {
                    println!("{}", self.pop());
                }

                Op::Jump(jump) => self.frame_mut().ip += jump as usize,

                Op::JumpBack(jump) => self.frame_mut().ip -= jump as usize,

                Op::PopJumpIfFalse(jump) => {
                    let jump = jump as usize;
                    if self.pop().is_falsey() {
                        self.frame_mut().ip += jump as usize;
                    }
                }

                Op::JumpIfFalseOrPop(jump) => {
                    let jump = jump as usize;
                    if self.peek(0).is_falsey() {
                        self.frame_mut().ip += jump as usize;
                    } else {
                        self.pop();
                    }
                }

                Op::JumpIfTrueOrPop(jump) => {
                    let jump = jump as usize;
                    if !self.peek(0).is_falsey() {
                        self.frame_mut().ip += jump as usize;
                    } else {
                        self.pop();
                    }
                }

                Op::Call(argc) => {
                    if !self.call_value(self.peek(argc as usize), argc) {
                        return InterpretResult::RuntimeError;
                    }
                }

                Op::Closure(k) => {
                    if let Value::Function(func) = self.get_constant(k) {
                        let mut closure = value::Closure::new(func);
                        for upvalue in closure.function.upvalues.borrow().iter() {
                            closure.upvalues.push(match upvalue {
                                (index, true) => self.capture_upvalue(*index as usize),
                                (index, false) => Rc::clone(
                                    &self.frame().closure.upvalues[*index as usize]
                                )
                            })
                        }
                        self.push(value::new_closure_value(closure))
                    } else {
                        unreachable!();
                    }
                }

                Op::CloseUpvalue() => {
                    self.close_upvalues(self.stack.len() - 1);
                    self.pop();
                }

                Op::Return() => {
                    let result = self.pop();
                    let frame = self.frames.pop().unwrap();
                    self.close_upvalues(frame.fp);
                    if self.frames.len() == 0 {
                        self.pop();
                        return InterpretResult::Ok;
                    }

                    while self.stack.len() > frame.fp {
                        self.pop();
                    }
                    self.push(result);
                }
            }
        }
    }

    fn read_op(&mut self) -> &Op {
        self.frame_mut().ip += 1;
        &self.frame().closure.function.chunk.code[self.frame().ip - 1]
    }

    fn get_constant(&self, k: u16) -> Value {
        self.frame().closure.function.chunk.constants[k as usize].clone()
    }

    fn get_string(&self, k: u16) -> String {
        if let Value::String(name) = self.get_constant(k) {
            (*name).clone()
        } else {
            unreachable!();
        }
    }

    fn capture_upvalue(&mut self, local: usize) -> value::UpvaluePtr {
        let mut prev_upvalue: &Option<value::UpvaluePtr> = &None;
        let mut upvalue = &self.open_upvalues;
        let index = local as usize + self.frame().fp;
        loop {
            match upvalue {
                None => break,
                Some(up) => {
                    match up.borrow().state {
                        value::UpvalueState::Open(location) => {
                            if location <= index {
                                break;
                            } else {
                                prev_upvalue = upvalue;
                                let temp = &up.borrow().next as *const Option<value::UpvaluePtr>;
                                // SAFETY: Dereferenced pointer was just
                                // created from valid data that we control
                                // in the previous line
                                upvalue = unsafe { &*temp };
                            };
                        }
                        value::UpvalueState::Closed(_) => unreachable!(),
                    }
                }
            }
        }
        if let Some(up) = upvalue {
            if let value::UpvalueState::Open(i) = up.borrow().state {
                if i == index {
                    return Rc::clone(&up);
                }
            }
        }
        let created_upvalue = value::new_upvalue(index);
        created_upvalue.borrow_mut().next = match upvalue {
            None => None,
            Some(up) => Some(Rc::clone(up)),
        };
        match prev_upvalue {
            None => self.open_upvalues = Some(Rc::clone(&created_upvalue)),
            Some(up) => up.borrow_mut().next = Some(Rc::clone(&created_upvalue)),
        }
        created_upvalue
    }

    fn close_upvalues(&mut self, last: usize) {
        while let Some(ref up) = self.open_upvalues {
            let up = Rc::clone(up);
            let mut upref = up.borrow_mut();
            match upref.state {
                value::UpvalueState::Open(location) => {
                    if location < last {
                        break;
                    }
                    upref.state = value::UpvalueState::Closed(self.stack[location].clone());
                    self.open_upvalues = match &upref.next {
                        None => None,
                        Some(next) => Some(Rc::clone(&next)),
                    };
                }
                value::UpvalueState::Closed(_) => unreachable!()
            }
        }
    }

    fn call_value(&mut self, callee: Value, argc: u8) -> bool {
        match callee {
            Value::Closure(closure) => self.call(closure, argc),
            Value::Native(native) => self.call_native(native, argc),
            _ => {
                runtime_error!(self, "Can only call functions and classes.");
                false
            }
        }
    }

    fn call(&mut self, closure: Rc<value::Closure>, argc: u8) -> bool {
        // Rust complains about mismatched types without the cast
        if argc as i32 != closure.function.arity {
            runtime_error!(self, "Expected {} arguments but got {}.", closure.function.arity, argc);
            return false;
        }
        if self.frames.len() == MAX_FRAMES {
            runtime_error!(self, "Stack overflow.");
            return false;
        }
        self.frames.push(CallFrame {
            closure,
            ip: 0,
            fp: self.stack.len() - argc as usize - 1,
        });
        true
    }

    fn call_native(&mut self, native: Rc<value::Native>, argc: u8) -> bool {
        if argc != native.arity {
            runtime_error!(self, "Expected {} arguments but got {}.", native.arity, argc);
            return false;
        }
        if self.frames.len() == MAX_FRAMES {
            runtime_error!(self, "Stack overflow.");
            return false;
        }
        let mut function = value::Function::new();
        function.name = "<native fn>".to_string();
        let closure = value::wrap_closure(function);
        self.frames.push(CallFrame {
            closure,
            ip: 0,
            fp: self.stack.len() - argc as usize - 1,
        });
        let result = (native.function)(self);
        while self.stack.len() > self.frame().fp {
            self.pop();
        }
        self.frames.pop();
        self.push(result);
        true
    }

    fn define_native(&mut self, name: String, function: value::NativeFn, arity: u8) {
        self.globals.insert(name, value::new_native(function, arity));
    }
}

fn clock_native(vm: &mut VM) -> Value {
    Value::Number(vm.start_time.elapsed().as_secs_f64())
}
