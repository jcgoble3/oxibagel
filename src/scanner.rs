use std::collections::HashMap;

pub type TokenResult = Result<TokenData, TokenError>;

#[derive(Clone)]
pub struct TokenError {
    pub message: String,
    pub line: i32,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Token {
    // Single-character tokens
    LeftParen, RightParen,
    LeftBrace, RightBrace,
    Comma, Dot, Semicolon,
    Plus, Minus, Star, Slash,
    // One or two character tokens
    Bang, BangEqual,
    Equal, EqualEqual,
    Greater, GreaterEqual,
    Less, LessEqual,
    // Literals
    Identifier, String, Number,
    // Keywords
    And, Class, Else, False, For, Fun, If, Nil, Or,
    Print, Return, Super, This, True, Var, While,
    // Others
    EndOfFile,
}

#[derive(Clone, Copy)]
pub struct TokenData {
    pub kind: Token,
    pub start: usize,
    pub end: usize,
    pub line: i32,
}

pub struct Scanner {
    pub source: Vec<u8>,
    srclen: usize,
    start: usize,
    current: usize,
    line: i32,
    keyword_map: HashMap<String, Token>,
}

impl Scanner {
    pub fn new(source: Vec<u8>) -> Scanner {
        Scanner {
            srclen: source.len(),
            source,
            start: 0,
            current: 0,
            line: 1,
            keyword_map: HashMap::from([
                (String::from("and"), Token::And),
                (String::from("class"), Token::Class),
                (String::from("else"), Token::Else),
                (String::from("false"), Token::False),
                (String::from("for"), Token::For),
                (String::from("fun"), Token::Fun),
                (String::from("if"), Token::If),
                (String::from("nil"), Token::Nil),
                (String::from("or"), Token::Or),
                (String::from("print"), Token::Print),
                (String::from("return"), Token::Return),
                (String::from("super"), Token::Super),
                (String::from("this"), Token::This),
                (String::from("true"), Token::True),
                (String::from("var"), Token::Var),
                (String::from("while"), Token::While),
            ]),
        }
    }

    pub fn scan_token(&mut self) -> TokenResult {
        self.skip_whitespace();

        self.start = self.current;

        if self.is_at_end() {
            return self.make_token(Token::EndOfFile);
        }

        let c = self.advance();

        match c {
            b'(' => self.make_token(Token::LeftParen),
            b')' => self.make_token(Token::RightParen),
            b'{' => self.make_token(Token::LeftBrace),
            b'}' => self.make_token(Token::RightBrace),
            b';' => self.make_token(Token::Semicolon),
            b',' => self.make_token(Token::Comma),
            b'.' => self.make_token(Token::Dot),
            b'+' => self.make_token(Token::Plus),
            b'-' => self.make_token(Token::Minus),
            b'*' => self.make_token(Token::Star),
            b'/' => self.make_token(Token::Slash),
            b'!' => if self.try_match(b'=') {
                self.make_token(Token::BangEqual)
            } else {
                self.make_token(Token::Bang)
            }
            b'=' => if self.try_match(b'=') {
                self.make_token(Token::EqualEqual)
            } else {
                self.make_token(Token::Equal)
            }
            b'<' => if self.try_match(b'=') {
                self.make_token(Token::LessEqual)
            } else {
                self.make_token(Token::Less)
            }
            b'>' => if self.try_match(b'=') {
                self.make_token(Token::GreaterEqual)
            } else {
                self.make_token(Token::Greater)
            }
            b'"' => self.string(),
            b'0' ..= b'9' => self.number(),
            b'a' ..= b'z' | b'A' ..= b'Z' | b'_' => self.identifier(),
            _ => self.error_token("Unexpected character.")
        }
    }

    fn advance(&mut self) -> u8 {
        self.current += 1;
        self.source[self.current - 1]
    }

    fn peek(&self) -> u8 {
        if self.is_at_end() {
            0
        } else {
            self.source[self.current]
        }
    }

    fn peek_next(&self) -> u8 {
        if self.is_at_end() {
            0
        } else {
            self.source[self.current + 1]
        }
    }

    fn try_match(&mut self, expected: u8) -> bool {
        if self.peek() != expected {
            return false;
        }
        self.advance();
        true
    }

    fn is_at_end(&self) -> bool {
        self.current == self.srclen
    }

    fn skip_whitespace(&mut self) {
        loop {
            let c = self.peek();
            if self.is_at_end() {
                return;
            }
            match c {
                b' ' | b'\r' | b'\t' => self.current += 1,
                b'\n' => {
                    self.line += 1;
                    self.advance();
                }
                b'/' => {
                    if self.peek_next() == b'/' {
                        while self.peek() != b'\n' && !self.is_at_end() {
                            self.advance();
                        }
                    } else {
                        return;
                    }
                }
                _ => return,
            }
        }
    }

    fn make_token(&self, kind: Token) -> TokenResult {
        Ok(TokenData {
            kind,
            start: self.start,
            end: self.current,
            line: self.line,
        })
    }

    pub fn error_token(&self, message: &str) -> TokenResult {
        Err(TokenError {
            message: String::from(message),
            line: self.line,
        })
    }

    fn string(&mut self) -> TokenResult {
        while self.peek() != b'"' && !self.is_at_end() {
            if self.peek() == b'\n' {
                self.line += 1;
            }
            self.advance();
        }

        if self.is_at_end() {
            self.error_token("Unterminated string.")
        } else {
            self.advance(); // closing quote
            self.make_token(Token::String)
        }
    }

    fn number(&mut self) -> TokenResult {
        while is_digit(&self.peek()) {
            self.advance();
        }
        if self.peek() == b'.' && is_digit(&self.peek_next()) {
            self.advance();
            while is_digit(&self.peek()) {
                self.advance();
            }
        }
        self.make_token(Token::Number)
    }

    fn identifier(&mut self) -> TokenResult {
        while is_alpha(&self.peek()) || is_digit(&self.peek()) {
            self.advance();
        }
        self.make_token(self.check_keyword())
    }

    fn check_keyword(&self) -> Token {
        let word = self.source[self.start..self.current].to_vec();
        match self.keyword_map.get(&String::from_utf8(word).unwrap()) {
            Some(token) => *token,
            None => Token::Identifier,
        }
    }
}

fn is_digit(c: &u8) -> bool {
    (b'0'..=b'9').contains(c)
}

fn is_alpha(c: &u8) -> bool {
    (b'a'..=b'z').contains(c) ||
    (b'A'..=b'Z').contains(c) ||
    *c == b'_'
}
