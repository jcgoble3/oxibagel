use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;

use crate::chunk;
use crate::vm;

#[derive(Clone, PartialEq)]
pub enum Value {
    Bool(bool),
    Nil,
    Number(f64),
    String(Rc<String>),
    Function(Rc<Function>),
    Closure(Rc<Closure>),
    Native(Rc<Native>),
}

impl Value {
    pub fn is_falsey(&self) -> bool {
        match self {
            Value::Nil | Value::Bool(false) => true,
            _ => false
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Bool(true) => write!(f, "true"),
            Value::Bool(false) => write!(f, "false"),
            Value::Nil => write!(f, "nil"),
            Value::Number(n) => write!(f, "{}", n),
            Value::String(string) => write!(f, "{}", string),
            Value::Function(func) => write!(f, "{}", func),
            Value::Closure(closure) => write!(f, "{}", closure.function),
            Value::Native(_) => write!(f, "<native fn>"),
        }
    }
}

pub fn new_string(string: String) -> Value {
    Value::String(Rc::new(string))
}

#[derive(Default)]
pub struct Function {
    pub arity: i32,
    pub chunk: chunk::Chunk,
    pub name: String,
    pub upvalues: RefCell<Vec<(u16, bool)>>,
}

impl Function {
    pub fn new() -> Self {
        Self {
            arity: 0,
            chunk: chunk::Chunk::new(),
            name: String::new(),
            upvalues: RefCell::new(Vec::new()),
        }
    }
}

pub fn new_function(function: Function) -> Value {
    Value::Function(Rc::new(function))
}

impl PartialEq for Function {
    fn eq(&self, other: &Function) -> bool {
        (self as *const Function) == (other as *const Function)
    }
}

impl fmt::Display for Function {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.name == "" {
            write!(f, "<script>")
        } else {
            write!(f, "<fn {}>", self.name)
        }
    }
}

pub enum UpvalueState {
    Open(usize), // holds absolute stack index
    Closed(Value), // holds value itself
}

pub struct Upvalue {
    pub state: UpvalueState,
    pub next: Option<UpvaluePtr>,
}

impl Upvalue {
    pub fn new(slot: usize) -> Self {
        Self {
            state: UpvalueState::Open(slot),
            next: None,
        }
    }
}

pub fn new_upvalue(slot: usize) -> UpvaluePtr {
    Rc::new(RefCell::new(Upvalue::new(slot)))
}

pub type UpvaluePtr = Rc<RefCell<Upvalue>>;

pub struct Closure {
    pub function: Rc<Function>,
    pub upvalues: Vec<Rc<RefCell<Upvalue>>>,
}

impl Closure {
    pub fn new(function: Rc<Function>) -> Self {
        Self {
            function,
            upvalues: Vec::new(),
        }
    }
}

pub fn new_closure_value(closure: Closure) -> Value {
    Value::Closure(Rc::new(closure))
}

pub fn wrap_closure(function: Function) -> Rc<Closure> {
    Rc::new(Closure::new(Rc::new(function)))
}

impl PartialEq for Closure {
    fn eq(&self, other: &Closure) -> bool {
        (self as *const Closure) == (other as *const Closure)
    }
}

pub type NativeFn = fn(vm: &mut vm::VM) -> Value;

pub struct Native {
    pub arity: u8,
    pub function: NativeFn,
}

impl Native {
    pub fn new(function: NativeFn, arity: u8) -> Self {
        Self {
            arity,
            function,
        }
    }
}

pub fn new_native(function: NativeFn, arity: u8) -> Value {
    Value::Native(Rc::new(Native::new(function, arity)))
}

impl PartialEq for Native {
    fn eq(&self, other: &Native) -> bool {
        (self as *const Native) == (other as *const Native)
    }
}
