use crate::value::Value;

#[derive(Copy, Clone)]
pub enum Op {
    Constant(u16),
    Nil(),
    True(),
    False(),
    Pop(),
    GetLocal(u16),
    SetLocal(u16),
    GetGlobal(u16),
    DefineGlobal(u16),
    SetGlobal(u16),
    GetUpvalue(u16),
    SetUpvalue(u16),
    Equal(),
    NotEqual(),
    Greater(),
    GreaterEqual(),
    Less(),
    LessEqual(),
    Add(),
    Subtract(),
    Multiply(),
    Divide(),
    Not(),
    Negate(),
    Print(),
    Jump(u16),
    JumpBack(u16),
    PopJumpIfFalse(u16),
    JumpIfFalseOrPop(u16),
    JumpIfTrueOrPop(u16),
    Call(u8),
    Closure(u16),
    CloseUpvalue(),
    Return(),
}

#[derive(Default)]
pub struct Chunk {
    pub code: Vec<Op>,
    pub lines: Vec<i32>,
    pub constants: Vec<Value>,
}

impl Chunk {
    pub fn new() -> Chunk {
        Chunk {
            code: Vec::new(),
            lines: Vec::new(),
            constants: Vec::new(),
        }
    }

    pub fn write(&mut self, op: Op, line: i32) {
        self.code.push(op);
        self.lines.push(line);
    }

    pub fn add_constant(&mut self, value: Value) -> usize {
        self.constants.push(value);
        self.constants.len() - 1
    }

    // Disassembler stuff

    pub fn disassemble(&self, name: &str) {
        println!("== {} ==", name);

        let mut offset = 0;
        while offset < self.code.len() {
            offset = self.dis_instruction(offset);
        }
    }

    pub fn dis_instruction(&self, offset: usize) -> usize {
        print!("{:04} ", offset);
        if offset > 0 && self.lines[offset] == self.lines[offset - 1] {
            print!("   | ");
        } else {
            print!("{:4} ", self.lines[offset]);
        }

        let instruction = self.code[offset];
        match instruction {
            Op::Constant(k) => self.dis_constant("Constant", k, offset),
            Op::Nil() => self.dis_simple("Nil", offset),
            Op::True() => self.dis_simple("True", offset),
            Op::False() => self.dis_simple("False", offset),
            Op::Pop() => self.dis_simple("Pop", offset),
            Op::GetLocal(v) => self.dis_u16("GetLocal", v, offset),
            Op::SetLocal(v) => self.dis_u16("SetLocal", v, offset),
            Op::GetGlobal(k) => self.dis_constant("GetGlobal", k, offset),
            Op::DefineGlobal(k) => self.dis_constant("DefineGlobal", k, offset),
            Op::SetGlobal(k) => self.dis_constant("SetGlobal", k, offset),
            Op::GetUpvalue(i) => self.dis_u16("GetUpvalue", i, offset),
            Op::SetUpvalue(i) => self.dis_u16("SetUpvalue", i, offset),
            Op::Equal() => self.dis_simple("Equal", offset),
            Op::NotEqual() => self.dis_simple("NotEqual", offset),
            Op::Greater() => self.dis_simple("Greater", offset),
            Op::GreaterEqual() => self.dis_simple("GreaterEqual", offset),
            Op::Less() => self.dis_simple("Less", offset),
            Op::LessEqual() => self.dis_simple("LessEqual", offset),
            Op::Add() => self.dis_simple("Add", offset),
            Op::Subtract() => self.dis_simple("Subtract", offset),
            Op::Multiply() => self.dis_simple("Multiply", offset),
            Op::Divide() => self.dis_simple("Divide", offset),
            Op::Not() => self.dis_simple("Not", offset),
            Op::Negate() => self.dis_simple("Negate", offset),
            Op::Print() => self.dis_simple("Print", offset),
            Op::Jump(jump) => self.dis_jump("Jump", jump, 1, offset),
            Op::JumpBack(jump) => self.dis_jump("Loop", jump, -1, offset),
            Op::PopJumpIfFalse(jump) => self.dis_jump("PopJumpIfFalse", jump, 1, offset),
            Op::JumpIfFalseOrPop(jump) => self.dis_jump("JumpIfFalseOrPop", jump, 1, offset),
            Op::JumpIfTrueOrPop(jump) => self.dis_jump("JumpIfTrueOrPop", jump, 1, offset),
            Op::Call(argc) => self.dis_u8("Call", argc, offset),
            Op::Closure(k) => self.dis_constant("Closure", k, offset),
            Op::CloseUpvalue() => self.dis_simple("CloseUpvalue", offset),
            Op::Return() => self.dis_simple("Return", offset),
        }
    }

    fn dis_simple(&self, name: &str, offset: usize) -> usize {
        println!("{}", name);
        offset + 1
    }

    fn dis_constant(&self, name: &str, constant: u16, offset: usize) -> usize {
        println!("{:-16} {:4} '{}'", name, constant, self.constants[constant as usize]);
        offset + 1
    }

    fn dis_u16(&self, name: &str, value: u16, offset: usize) -> usize {
        println!("{:-16} {:4}", name, value);
        offset + 1
    }

    fn dis_u8(&self, name: &str, value: u8, offset: usize) -> usize {
        println!("{:-16} {:4}", name, value);
        offset + 1
    }

    fn dis_jump(&self, name: &str, jump: u16, sign: i32, offset: usize) -> usize {
        let target = (offset as i32) + 1 + sign * (jump as i32);
        println!("{:-16} {:4} (to {})", name, jump, target);
        offset + 1
    }
}
