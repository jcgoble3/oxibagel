# Chapter 25: Closures

- [Link to book chapter](https://craftinginterpreters.com/closures.html)
- [Previous chapter](24-calls-and-functions.md)
- [Table of Contents](index.md)
- [Next chapter](26-garbage-collection.md)

## 25.1: Closure Objects

- Simple stuff so far.
- Helper factory functions are nice for hiding the messy wrapping of `Rc` boxes.

## 25.2 Upvalues

- Because of our `enum`-based approach to opcodes, the variable number of operands to the `Closure` opcode doesn't work here. Instead, we store that data on the function object itself, where it fits nicely.
- `RefCell` allows us to have a field that can be mutated without having to borrow the entire object mutably and causing all sorts of borrow checker issues. `RefCell` does not allow you to *violate* the borrowing rules, though; rather, it checks the rules at runtime, adding a bit of overhead.
- Here, we use it for the `upvalues` field on a `Function`, since we need to pass around references to `self.compiler` just for that purpose.
- We also go back and enclose the parser's `had_error` field in a `RefCell`, which in turn allows us to make the error function receive `&self` instead of `&mut self`, along with a few functions which took `&mut self` only because they could throw errors.

## 25.3 Upvalue Objects

- Because we don't have pointers into the stack here, upvalues here are a blend of an enum for open/closed and (eventually) a linked list pointer. Since Upvalues can be referenced from several places, we need `Rc`, but they can also be mutated, so we really need `Rc<RefCell>`, which is our first use of this pattern.
- I cheaped out a bit on getting a reference to the upvalue's value. Writing a VM method was giving me fits because of borrowing issues, but surprisingly it was much cleaner to write with a macro.
- Note in the match statement the use of `ref mut` in the `Closed` arm. This means to make the given name (`value`) a mutable reference to the value in that position, instead of consuming the value. This allows us to later assign to the dereferenced location, changing the value inside the enum.

## 25.4 Closed Upvalues

- We get another `RefCell` with the `Local::is_captured` flag, because borrowing issues are too complex to take a mutable reference to the whole `compiler`.
- The `capture_upvalue` function proved problematic to write. In that function, we need to walk a linked list, tracking the previous and current upvalues, while checking internal data of the current upvalue. Since the value being tracked is a `&Option<Rc<RefCell<Upvalue>>>`, we first off end up with a bunch of boilerplate to match the `Option` cases, then more matching on the upvalue state. More importantly though, the presence of the `RefCell` leads to a major problem with the borrow checker that ultimately proved to be intractable. We know that what we're doing with walking the linked list is safe, but Rust can't understand it. Unfortunately, Rust's safety rules necessarily make some safe code impossible to avoid accepting unsafe behavior. So when we know our code is okay, but Rust doesn't, it's time to reach for `unsafe` code.
- Here, we were not able to extract a reference to the `next` object using only safe Rust. However, a small `unsafe` block solves the problem cleanly. The unsafe action here is dereferencing a raw pointer; Rust can't determine whether a raw pointer points to valid data, so dereferencing a raw pointer always requires an `unsafe` block. But we know it's safe! Specifically, we can clearly see that the pointer is created in the immediately preceding line of code from a reference to valid data that we control, and is converted back into a reference of the same type. Hence, there are no actual safety issues.
