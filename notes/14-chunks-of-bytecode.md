# Chapter 14: Chunks of Bytecode

- [Link to book chapter](https://craftinginterpreters.com/chunks-of-bytecode.html)
- [Table of Contents](index.md)
- [Next chapter](15-a-virtual-machine.md)

## 14.1: Bytecode?

- Just general concepts, nothing of note to Rust

## 14.2: Getting Started

- The little bit of setup work here is significantly different in Rust:
  - Rust does not require headers or function prototypes, since the project is compiled as a whole instead of as individual source files that are subsequently linked together in a separate step.
  - Rust's `main()` does not take arguments; instead, one must reach into the Rust standard library to get the command-line arguments using `std::env`.
  - The functionality of `stdbool.h` and `stdint.h` is present by default in Rust.
  - The items needed from `stddef.h` are (in C terms) `size_t` and `NULL`. Rust has a default equivalent type to `size_t`, and no concept of `NULL` pointers (well, okay, technically it does, but only in `unsafe` code, which should be avoided if possible). A different pattern is used as we will soon see.

## 14.3: Chunks of Instructions

- Rust does require that modules be declared and imported.
- Modules do not have to be in separate files, but we will use one file per module here anyway.
- We will keep in simple and have all modules in the `src/` directory. To do that, all top-level module *declarations* go at the top of `main.rs`. Then, each individual module can import the sibling modules it needs via `use` statements.
- Enums are massively different in Rust compared to C:
  - Rust enums are objects that may take arguments (or may not), and are integrated into the type system.
  - Whereas in C, enums simply are `int`s, in Rust an argument-less enum can be annotated as a integer representation, and then its values can be *cast* to the given integer type. However, you cannot cast an integer back to an enum value directly; a manual operation is required for that.
- Since we will be interleaving named opcodes and arbitrary bytes in a single array, we need to define that manual operation.
- Rust requires `match` statements to be completely exhaustive, which means our manual cast to the enum must handle every integer from 0 to 255. To do that, the standard idiom would be to return a `Result` enum, which is either `Ok(actual value)` or `Err` for an error. But that then has to be unwrapped every single place that works with opcodes, which is likely to get messy. Instead, we define an extra opcode `INVALID` and just return that so there is no unwrapping, and handle `INVALID` in the few places where it is needed.
- Rust takes one C worry off our minds: the built-in `Vec` abstracts all of the messes of dynamic arrays away for us.
  - This reduces the body of `writeChunk` in C of 8 lines down to a single line in Rust with `Chunk::write`.
- Rust's memory safety means we don't ever perform manual memory allocation or deallocation, so we don't even need the `memory` module, at least for now. We also don't need `freeChunk`.

## 14.4: Disassembling Chunks

- Rust variables are immutable (constant) by default. In order to mutate a variable, the variable must be declared as mutable with the `mut` keyword. `chunk.write()` tells rust that it mutates the chunk, so `main()` must declare it as mutable.
- We won't use a `debug` module here; everything in the debug module is conceptually an operation on a `Chunk`, and therefore belongs as methods of `Chunk`.
- Rust has implicit returns at the end of a function: if the last line executed is an expression without a semicolon at the end, it is evaluated and becomes the return value.

## 14.5: Constants

- Rust's `typedef`s look like simple assignment, making it nice and clear which is the existing type name and which is the new one.
- `Vec` once again saves us dozens of lines of boilerplate again for dynamic arrays.

## 14.6: Line Information

- `Vec` saves us again, but otherwise nothing really new or notable
