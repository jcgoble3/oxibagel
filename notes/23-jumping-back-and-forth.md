# Chapter 23: Jumping Back and Forth

- [Link to book chapter](https://craftinginterpreters.com/jumping-back-and-forth.html)
- [Previous chapter](22-local-variables.md)
- [Table of Contents](index.md)
- [Next chapter](24-calls-and-functions.md)

## 23.1: If Statements

- I chose to mix up the jump instructions a bit, particularly with JumpIfFalse. That almost always needs a Pop immediately after on both branches, except on the logical operators, so I'd rather have separate instructions for each case.

## 23.2: Logical Operators

- The new jump instructions are named after the corresponding opcodes in Python.

## 23.3: While Statements

- In the clox notes, I mentioned that I would probably rename `OP_LOOP` to `OP_JUMP_BACK`. I did that here with `Op::JumpBack`.

## 23.4: For Statements

- Rust conversion here is simple.
