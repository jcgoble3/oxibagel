# Chapter 17: Compiling Expressions

- [Link to book chapter](https://craftinginterpreters.com/compiling-expressions.html)
- [Previous chapter](16-scanning-on-demand.md)
- [Table of Contents](index.md)
- [Next chapter](18-types-of-values.md)

## Lede

- clox used a Pratt parser for expressions, and switched to recursive descent for statements. The Java section of the book used recursive descent for everything. I strongly dislike Pratt parsing, so I am instead going to try my hand at rewriting the expression parser with recursive descent, with as few looks at the Java section as possible.
- I'm also going to switch to opcode enum variants carrying their own operands. It will make the code simpler, at the expense of a little bit of data locality. If and when register-based opcodes are implemented, the data locality issues will be eliminated.

## 17.1: Single-Pass Compilation

- Yay, more theory.

## 17.2: Parsing Tokens

- Pretty simple.
- I did have to pull some gymnastics with cloning objects because of issues surrounding double borrows that the Rust borrow checker didn't understand was okay.

## 17.3: Emitting Bytecode

- The chunk, at least for now, is owned by the VM, so we have to store a mutable reference to it in the parser. To store a reference in a struct requires explicit lifetime parameters, hence this ugly `<'a>` code sprinkled around.

## 17.4/17.5: but with recursive descent

- I just find recursive descent to be so much simpler and easier to understand than Pratt parsing. No bouncing around among various function, just a descent through the "stack" of functions and a climb back up. Isn't this expression parser so much easier to understand?
- We have a few extra functions here from the grammar; we'll implement those when we get to those types of expressions in the book.
