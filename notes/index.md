# Notes on Rust adaptation of clox

The files in this folder will be used to make notes about adapting clox from [the book](https://craftinginterpreters.com/) to Rust.
The notes here are specific to how to implement Lox with Rust, and about Rust itself.
For notes on the general language implementation concepts themselves, please see the `notes` folder in [my clox implementation](https://gitlab.com/jcgoble3/cbagel).

## Table of Contents

Notes are organized by book chapter.
Some links below may not work if the relevant chapter has not been completed yet.

- [Chapter 14: Chunks of Bytecode](14-chunks-of-bytecode.md)
- [Chapter 15: A Virtual Machine](15-a-virtual-machine.md)
- [Chapter 16: Scanning on Demand](16-scanning-on-demand.md)
- [Chapter 17: Compiling Expressions](17-compiling-expressions.md)
- [Chapter 18: Types of Values](18-types-of-values.md)
- [Chapter 19: Strings](19-strings.md)
- [Chapter 20: Hash Tables](20-hash-tables.md)
- [Chapter 21: Global Variables](21-global-variables.md)
- [Chapter 22: Local Variables](22-local-variables.md)
- [Chapter 23: Jumping Back and Forth](23-jumping-back-and-forth.md)
- [Chapter 24: Calls and Functions](24-calls-and-functions.md)
- [Chapter 25: Closures](25-closures.md)
- [Chapter 26: Garbage Collection](26-garbage-collection.md)
- [Chapter 27: Classes and Instances](27-classes-and-instances.md)
- [Chapter 28: Methods and Initializers](28-methods-and-initializers.md)
- [Chapter 29: Superclasses](29-superclasses.md)
- [Chapter 30: Optimization](30-optimization.md)
