# Chapter 16: Scanning on Demand

- [Link to book chapter](https://craftinginterpreters.com/scanning-on-demand.html)
- [Previous chapter](15-a-virtual-machine.md)
- [Table of Contents](index.md)
- [Next chapter](17-compiling-expressions.md)

## 16.1: Spinning Up the Interpreter

- Not a lot to say here, except that Rust forces explicit handling of file I/O errors by returning a `Result` that you have to `match` or unwrap.

## 16.2: A Token at a Time

- We have a roadblock here: the code in the book requires examining a single character at a time. However, Rust `String`s are Unicode, encoded in UTF-8, so the model we've used up until now doesn't make sense because you can't index characters in a UTF-8 string at O(1). To solve this, we're going to switch to byte-strings instead.
- However, in order to print out strings, we still have to deal with UTF-8 because everything in the Rust standard library assumes UTF-8. So OxiBagel will have one additional restriction from CBagel: all source files must be fully UTF-8 encoded.
- For token data, since we are using slices instead of C's `printf`, it makes more sense (at least for now) to store the end index of the token rather than the length.
- Rust enums aren't just scalar values. THey can also have tuple-like arguments and values contained within. We see that here in that error messages are carried in the payload of the `Token::Error` itself.
- Speaking of which, let's talk about pattern matching. Pattern matching is kind of like a `switch` statement (and you can certainly use it as one), except it's much more powerful. Matching lets you match on types (like enum variants) and extract values from them and bind them to names in your scope. Here in `compiler::compile`, I extracted a reference to the error message and then used it in the `println!` call. The `ref` keyword tells Rust to match the value but produce a reference to that value. There's much more that can be done with pattern matching as well.
  - In fact, if I wanted to, I could make all the `Op` enum variants hold their own operands, and then the code vector becomes a `Vec<Op>` instead of a `Vec<u8>`. But that might be bad for data locality and could slow down the VM. It's worth investigating, though as it would let me drop a set of heavy crate dependencies.

## 16.3: A Lexical Grammar for Lox

- Converting from one language to another has a particular challenge in that each language has different reserved keywords. Previously in this chapter, we had to use `kind` instead of `type` in the `TokenData` struct becase `type` is a keyword in Rust. Now here, the scanner's `match` function becomes `try_match` because `match` is a Rust keyword.
- In C, it's okay for us to index one past the end of the source code (or dereference a pointer to such an index) because that character is guaranteed to be a `'\0'`. But in Rust, indexing one past the end of the vector of bytes is an error, so we have to do some extra checks of whether we are at the end.
- `is_digit` takes advantage of Rust's range syntax: `x .. y` is the range from and including `x` to, but not including, `y`. To include the second endpoint, we add an `=`, so it becomes `x ..= y`, which is the range from `x` to `y` including both endpoints.

## 16.4: Identifiers and Keywords

- Nothing to note in terms of Rust.
