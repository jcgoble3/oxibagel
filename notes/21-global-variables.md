# Chapter 21: Global Variables

- [Link to book chapter](https://craftinginterpreters.com/global-variables.html)
- [Previous chapter](20-hash-tables.md)
- [Table of Contents](index.md)
- [Next chapter](22-local-variables.md)

## 21.1: Statements

- Pretty straightforward stuff.
- I decided to leave out error synchronization entirely. Lua, Python, and Ruby all report only the first syntax error and leave it at that. I find that even in languages that try to report multiple errors, rarely is any error beyond the first actually meaningful. So instead of trying to synchronize and report multiple errors, we'll just bail at the first syntax error.

## 21.2: Variable Declarations

- We get our first use of Rust's standard library HashMaps for storing global variables.

## 21.3: Reading Variables

- We see here the way Rust handles a potential null: with the `Option<T>` enum and its variants `Some(T)` and `None`. Like any other enum, you must explicitly check every case.

## 21.4: Assignment

- Handling when assignment is allowed and when it isn't is a tricky one with our conversion to recursive descent.
