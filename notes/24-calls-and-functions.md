# Chapter 24: Calls and Functions

- [Link to book chapter](https://craftinginterpreters.com/calls-and-functions.html)
- [Previous chapter](23-jumping-back-and-forth.md)
- [Table of Contents](index.md)
- [Next chapter](25-closures.md)

## 24.1: Function Objects

- Rust makes it difficult to compare objects by identity, but instead expects comparison by contents. In order to compare by identity, as functions are compared in Lox, both operands must be cast to raw pointers.

## 24.2: Compiling to Function Objects

- Rust doesn't like taking ownership of a struct field when that struct is behind a `&mut` reference. We have to pull some shenanigans with `std::mem::take` and the `Default` trait to convince the compiler to let us have ownership of the `Function` here.
- We also see here that even `if`/`else` statements are expressions with values in Rust.

## 24.3: Call Frames

- We lose the benefit of `frame` being a local variable here, because it would need to be a mutable borrow, which instantly blocks every other borrow we would do in `run()`. So we have to go through the frame array every time, which we hide behind a method.
- This created some borrowing issues surrounding the instruction matching code, for which the easiest solution was to take `Op` and make it `Copy` so it can be copied and moved around at will instead of needing to borrow repeatedly.

## 24.4: Function Declarations

- `Box` is basically a pointer that owns the object it's pointing to. It serves as a layer of indirection to make things like recursive structs possible. Here, we use it for a linked list of compiler objects.

## 24.5: Function Calls

- Pretty straightforward implementation.

## 24.6: Return Statements

- Simple stuff.

## 24.7: Native Functions

- Annoyingly, Rust doesn't have a simple `clock()` function, so we have to store a `time::Instant` and get the elapsed time from it instead.
- I also added arity checking for native functions.
- I had to change up the native calling convention here. I needed to pass the VM, so that the `clock()` function could access the `start_time`. But in order to do that, I was passing `self`, which is a mutable borrow, and the slice of arguments, which was an immutable borrow. And that's a no-no. Since passing `self` was not optional, I couldn't pass the arguments directly. Therefore native functions get their own stack frame, and access their Lox arguments through API functions that don't exist yet. That's fine here since `clock()` doesn't take any arguments, but it's the first thing that will be needed for a future standard library.
  - Interestingly, this is how I really wanted native functions to be implemented anyway: with just an opaque "state" object as the only argument, just like in Lua. I didn't plan on being forced to do it this soon, though.
