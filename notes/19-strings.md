# Chapter 19: Strings

- [Link to book chapter](https://craftinginterpreters.com/strings.html)
- [Previous chapter](18-types-of-values.md)
- [Table of Contents](index.md)
- [Next chapter](20-hash-tables.md)

## 19.1: Values and Objects

- Since OxiBagel will probably not have a garbage collector, we need to think about how to share objects. We need multiple ownership *and* mutability for most objects. We do that by combining `Rc` for reference counting and `RefCell` for *interior mutability*. Both are called "smart pointers" because they point to data on the heap in a safe way (guaranteed to be valid).
- Since `Rc` only allows immutable borrows, the `RefCell` allows for mutable borrows. We can't violate the normal borrowing rules, though; they are still checked, but at runtime instead of compile time, meaning that we will get a panic if we violate the normal rules (specifically, that we can either have any number of immutable borrows *or* one mutable borrow at the same time, but never both and never two mutable borrows). So it's important for us to be careful in writing code to avoid a panic at runtime.
- However, `RefCell` might not be needed for strings, since strings in Lox are immutable.

## 19.2: Struct Inheritance

- Most of this section is C-specific and not applicable to our Rust implementation. We do, however, lose the `Copy` trait on `Value`, meaning that we'll have to clone `Value`s or pass references around. Except for simple cases like `print_value`, it's probably better to clone to reduce the overhead of indirection.
- Actually, instead of remembering to use a reference when calling `print_value`, it's better to just implement the `Display` trait to allow direct printing and conversions to strings of `Value`s. So I did.

## 19.3: Strings

- Again, a lot of C code eliminated by using Rust built-ins.

## 19.4: Operations on Strings

- Concatenation is also extremely simple in Rust compared to C.

## 19.5: Freeing Objects

- And we don't need to do any of this here, since we are relying on Rust's reference-counting smart pointers instead. We just have to watch out for reference cycles later on, but strings don't refer to any other objects, so we're safe for a while.
