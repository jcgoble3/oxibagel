# For convenience, copy the interpreter to the top level.
bagel: target/debug/bagel
	@ cp target/debug/bagel bagel

# Compile the OxiBagel interpreter.
target/debug/bagel: src/*.rs
	@ cargo build

# Debug builds with tracing features
vmtrace:
	@ cargo build --features vmtrace
	@ cp target/debug/bagel bagel

printcode:
	@ cargo build --features printcode
	@ cp target/debug/bagel bagel

# Run the tests for oxibagel.
test: setup bagel
	@ ./util/test.py chap25_closures

# Create the Python environment and install packages into it.
setup:
	@ if [ ! -d util/env ]; then python3 -m venv util/env; fi

# Remove all build outputs and intermediate files.
clean:
	@ cargo clean
	@ rm -f ./bagel

release:
	@ cargo build --release

.PHONY: clean release setup test vmtrace printcode
